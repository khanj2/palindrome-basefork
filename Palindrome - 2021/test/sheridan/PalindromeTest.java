package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		assertTrue( "Provided string failed palindrome validation"
				, Palindrome.isPalindrome("anna") );
	}

	@Test
	public void testIsPalindromeException( ) {
		assertFalse( "Provided string passed palindrome validation"
				, Palindrome.isPalindrome("this is not a palindrome") );
	}
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue( "Provided string failed palindrome validation", Palindrome.isPalindrome("Race car ") );
	}
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse( "Provided string failed palindrome validation"
				, Palindrome.isPalindrome("don'tnod") );
	}	

}
